/*
    File:    test_int128_to_str.cpp
    Created: 27 March 2019 at 09:27 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include <iostream>
#include <utility>
#include "int128_to_str.h"
#include "testing.hpp"

static const unsigned __int128 first  = 34028236692093846ULL;
static const unsigned __int128 second = 34633746074317682ULL;
static const unsigned __int128 third  = 11455ULL;
static const unsigned __int128 fourth = first * 1'000'000'000'000'000'00ULL + second;
static const unsigned __int128 fifth  = fourth * 100'000ULL + third;

using Pair_for_test = std::pair<unsigned __int128, std::string>;

static const Pair_for_test tests[] = {
    {first, "34028236692093846"},                       {second, "34633746074317682"                 },
    {third, "11455"            },                       {fourth, "3402823669209384634633746074317682"},
    {fifth, "340282366920938463463374607431768211455"},
};

int main()
{
    std::cout << "first (34028236692093846):                       " << to_string(first)  << std::endl
              << "second (34633746074317682):                      " << to_string(second) << std::endl
              << "third (11455):                                   " << to_string(third)  << std::endl
              << "fourth (3402823669209384634633746074317682):     " << to_string(fourth) << std::endl
              << "fifth (340282366920938463463374607431768211455): " << to_string(fifth)  << std::endl;


    std::cout << "\nRunning tests for conversion...\n";
    testing::test(std::begin(tests), std::end(tests), to_string);

    return 0;
}