LINKER      = g++
LINKERFLAGS = -s
CXX         = g++
CXXFLAGS    = -O1 -Wall -std=gnu++14 -Wextra
BIN         = test-conv
vpath %.o build
OBJ         = test_int128_to_str.o int128_to_str.o
LINKOBJ     = build/test_int128_to_str.o build/int128_to_str.o

.PHONY: all all-before all-after clean clean-custom

all: all-before $(BIN) all-after

clean: clean-custom
	rm -f ./build/*.o
	rm -f ./build/$(BIN)

.cpp.o:
	$(CXX) -c $< -o $@ $(CXXFLAGS)
	mv $@ ./build

$(BIN):$(OBJ)
	$(LINKER) -o $(BIN) $(LINKOBJ) $(LINKERFLAGS)
	mv $(BIN) ./build
